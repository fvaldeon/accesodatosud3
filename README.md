  - **VehiculosHibernate**: Operaciones Crud con Hibernate y gesti�n relaci�n 1:N
  - **Liga_futbol**: Aplicacion liga_futbol con una relacion 1:N y dos N:M. FocusListener, ItemListener, ChangeListener, y los comunes. Tablas, listas, ComboBoxes y Cuadros de dialogo. Clases Mapeadas con pluggin Hibernate
  - **Registro_Homicidas**: Aplicacion con una base de datos con dos tablas relaci�n 1:N, operaciones CRUD, JList, JComboBox, gr�fico JFreeChart. Permite conectar con MySql y PostgreSql. Clases mapeadas con pluggin Hibernate
  - **Colegio**: Aplicaci�n con relaci�n N:M (alumnos-asignaturas) y 1:N (asignaturas-profesor) bidireccionales. Clases mapeadas a mano.

  En la secci�n [Wiki](https://bitbucket.org/fvaldeon/accesodatosud3/wiki/Home) de este repositorio se detalla el funcionamiento de algunas aplicaciones.
