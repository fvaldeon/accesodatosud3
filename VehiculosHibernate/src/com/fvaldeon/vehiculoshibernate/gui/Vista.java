package com.fvaldeon.vehiculoshibernate.gui;

import com.github.lgooddatepicker.components.DateTimePicker;

import javax.swing.*;

/**
 * Created by fvaldeon on 12/01/2018.
 */
public class Vista {
    private JFrame frame;
    private JPanel panel1;
    JTextField txtMatricula;
    JTextField txtMarca;
    JTextField txtModelo;
    JTextField txtId;
    DateTimePicker dateTimePicker;
    JList listCoches;
    JButton altaButton;
    JButton listarButton;
    JButton modificarButton;
    JButton borrarButton;
    JTextField txtPropietario;
    JList listPropietarios;
    JList listCochesPropietario;
    JButton listarPropietariosButton;
    DefaultListModel dlm;
    DefaultListModel dlmPropietarios;
    DefaultListModel dlmCochesPropietario;
    JMenuItem conexionItem;
    JMenuItem salirItem;

    public Vista(){
        frame = new JFrame("Vista");
        frame.setContentPane(panel1);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        crearMenu();
        frame.pack();
        frame.setVisible(true);

        crearModelos();

        frame.setLocationRelativeTo(null);
    }

    private void crearModelos() {
        dlm = new DefaultListModel();
        listCoches.setModel(dlm);
        dlmPropietarios = new DefaultListModel();
        listPropietarios.setModel(dlmPropietarios);
        dlmCochesPropietario = new DefaultListModel();
        listCochesPropietario.setModel(dlmCochesPropietario);
    }

    private void crearMenu() {
        JMenuBar barra = new JMenuBar();
        JMenu menu = new JMenu("Archivo");

        conexionItem = new JMenuItem("Conectar");
        conexionItem.setActionCommand("Conectar");

        salirItem = new JMenuItem("Salir");
        salirItem.setActionCommand("Salir");

        menu.add(conexionItem);
        menu.add(salirItem);
        barra.add(menu);
        frame.setJMenuBar(barra);
    }


}
