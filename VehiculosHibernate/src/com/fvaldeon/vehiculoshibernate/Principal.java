package com.fvaldeon.vehiculoshibernate;

import com.fvaldeon.vehiculoshibernate.gui.Controlador;
import com.fvaldeon.vehiculoshibernate.gui.Modelo;
import com.fvaldeon.vehiculoshibernate.gui.Vista;

/**
 * Created by fvaldeon on 12/01/2018.
 */
public class Principal {
    public static void main(String[] args) {
        Vista vista = new Vista();
        Modelo modelo = new Modelo();
        Controlador controlador = new Controlador(vista,modelo);
    }
}
